//
//  ContentView.swift
//  nobu-neo
//
//  Created by Prima Jatnika on 18/11/21.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject var videoCallVM = ImmediateVcViewModel()
    
    @State private var channelCtlr: String = ""
    @State private var toggleUrl: Bool = false
    
    @State private var isShowJitsi: Bool = false
    
    var body: some View {
        NavigationView {
            VStack {
                
                NavigationLink(
                    destination: JitsiScreen(
                        jitsiRoom: .constant(self.videoCallVM.roomName),
                        url: .constant(toggleUrl ? AppConstants().VCS_URL_GCP : AppConstants().VCS_URL_OCP)),
                    isActive: $isShowJitsi) {
                    EmptyView()}
                
                VStack(alignment: .leading, spacing: 20) {
                    Toggle(isOn: self.$toggleUrl, label: {
                        Text("\(toggleUrl ? "GCP" : "OCP")")
                            .fontWeight(.semibold)
                    })
                    .padding(.bottom, 20)
                    
                    VStack {
                        TextField("Room Name (NIK-CHANNEL-DEVICE)",
                                  text: self.$channelCtlr)
                        Divider()
                    }
                    
                    Button(action: {
                        submitVcall()
                    }, label: {
                        Text(disableButton ? "Mohon Tunggu" : "SUBMIT")
                            .font(.subheadline)
                            .fontWeight(.bold)
                            .foregroundColor(Color.white)
                            .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                    })
                    .disabled(disableButton)
                    .background(disableButton ? Color.gray : Color.blue)
                    .cornerRadius(5)
                    .padding(.bottom, 5)
                    
                    Text("{\(self.videoCallVM.message)}")
                        .font(.caption)
                }
                .padding(.horizontal)
            }
        }
        .onReceive(NotificationCenter.default.publisher(for: NSNotification.Name("ParticipanLeave"))) { obj in
            if let rc = obj.userInfo, let info = rc["rc"] {
                print("RC \(info)")
                self.videoCallVM.message = "{ 'rc': '\(info)' }"
            }
        }
        .alert(isPresented: self.$videoCallVM.showAlert, content: {
            return Alert(
                title: Text("Pesan"),
                message: Text("\(self.videoCallVM.message)"),
                dismissButton: .cancel()
            )
        })
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

extension ContentView {
    
    var disableButton: Bool {
        channelCtlr.isEmpty || self.videoCallVM.isLoading
    }
    
    func submitVcall() {
        
        let splitData = channelCtlr.split(separator: "-")
        let nik = splitData[0]
        let channel = splitData[1]
        
        print(nik, channel)
        
        self.videoCallVM.immediateVc(
            nik: "\(nik)",
            channel: "\(channel)",
            switchUrl: toggleUrl) { success in
            
            if success {
                print("Success & Show JITSI")
                self.isShowJitsi = true
            }
            
            if !success {
                
            }
        }
    }
    
}
