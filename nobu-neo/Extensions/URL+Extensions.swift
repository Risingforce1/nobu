//
//  URL+Extensions.swift
//  nobu-neo
//
//  Created by Prima Jatnika on 18/11/21.
//

import Foundation

extension URL {
    
    static func urlImidiateVCallOCP() -> URL? {
        return URL(string: AppConstants().BASE_URL_OCP + "/customer/immediateVC")
    }
    
    static func urlImidiateVCallGCP() -> URL? {
        return URL(string: AppConstants().BASE_URL_GCP + "/customer/immediateVC")
    }
}

extension URL {
    
    // MARK: Add Query Param
    func appending(_ queryItem: String, value: String?) -> URL {

        guard var urlComponents = URLComponents(string: absoluteString) else { return absoluteURL }

        var queryItems: [URLQueryItem] = urlComponents.queryItems ??  []
        let queryItem = URLQueryItem(name: queryItem, value: value)
        queryItems.append(queryItem)
        urlComponents.queryItems = queryItems

        return urlComponents.url!
    }
}
