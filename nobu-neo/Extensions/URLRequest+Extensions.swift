//
//  URLRequest+Extensions.swift
//  nobu-neo
//
//  Created by Prima Jatnika on 18/11/21.
//

import Foundation

extension URLRequest {
    
    init(_ url: URL) {
        self.init(url: url)
        self.timeoutInterval = 60
        self.setValue("*/*", forHTTPHeaderField: "accept")
        self.setValue("application/json", forHTTPHeaderField: "Content-Type")
        self.setValue("TEST12345", forHTTPHeaderField: "X-Device-ID")
    }
}
