//
//  ImmediateVcViewModel.swift
//  nobu-neo
//
//  Created by Prima Jatnika on 18/11/21.
//

import Foundation

class ImmediateVcViewModel: ObservableObject {
    @Published var isLoading: Bool = false
    @Published var showAlert: Bool = false
    @Published var message: String = ""
    
    @Published var jsonResponse: String = ""
    @Published var roomName: String = ""
}

extension ImmediateVcViewModel {
    
    func immediateVc(nik: String, channel: String, switchUrl: Bool, completion: @escaping (Bool) -> Void) {
        
        ImmediateVcService.shared.postImmediateVc(nik: nik, channel: channel, urlSwitch: switchUrl) { result in
            
            self.isLoading = true
            
            switch result {
            case .success(let response):
                
                if (response.code == "200") {
                    
                    self.isLoading = false
                    self.roomName = response.roomName ?? ""
                    
                    completion(true)
                } else {
                    
                    self.isLoading = false
                    self.showAlert = true
                    self.message = response.message ?? ""
                    
                    completion(false)
                }
                
                break
            
            case .failure(let error):
                
                self.isLoading = false
                self.showAlert = true
                self.message = "\(error)"
                
                print(error)
                completion(false)
                break
            
            }
        }
    }
}
