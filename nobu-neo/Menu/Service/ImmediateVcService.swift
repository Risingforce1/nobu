//
//  ImmediateVcService.swift
//  nobu-neo
//
//  Created by Prima Jatnika on 18/11/21.
//

import Foundation

class ImmediateVcService {
    
    private init() {}
    static let shared = ImmediateVcService()
    
    // MARK:- POST
    func postImmediateVc(
        nik: String,
        channel: String,
        urlSwitch: Bool,
        completion: @escaping(Result<ImmediateVcResponseModel, ErrorResult>) -> Void) {
        
        print(nik, channel)
        
        guard let url = urlSwitch ? URL.urlImidiateVCallGCP() : URL.urlImidiateVCallOCP() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        let body: [String: Any] = [
            "nik": nik
        ]
        
        var request = URLRequest(url.appending("channel", value: channel))
        request.httpMethod = "POST"
        
        do {
            // MARK : serialize model data
            let jsonData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            _ = String(data: jsonData, encoding: String.Encoding.ascii)
            
            request.httpBody = jsonData
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.parser(string: "ERROR PARSE BODY")))
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(ImmediateVcResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    print("RC Error: \(rc.statusCode)")
                    if let value = try? JSONDecoder().decode(ImmediateVcResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
}
