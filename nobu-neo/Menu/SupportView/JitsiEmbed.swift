//
//  JitsiEmbed.swift
//  nobu-neo
//
//  Created by Prima Jatnika on 18/11/21.
//

import Foundation
import UIKit
import SwiftUI
import JitsiMeetSDK

struct JitsiEmbed: UIViewRepresentable {
    
    @Binding var jitsi_room: String
    @Binding var status: String
    @Binding var url: String
    
    @State var JMView: JitsiMeetView?
    let JMDel = JMDelegate(jitsiView: nil, viewSelf: nil)
    
    public func makeUIView(context: Context) -> JitsiMeetView {
        
        let defaultOptions = JitsiMeetConferenceOptions.fromBuilder { (builder) in
            builder.serverURL = URL(string: url)
            builder.welcomePageEnabled = false
            builder.setFeatureFlag("invite.enabled", withBoolean: false)
            builder.setFeatureFlag("pip.enabled", withValue: false)
            builder.setFeatureFlag("recording.enabled", withValue: false)
            builder.setFeatureFlag("close-captions.enabled", withValue: false)
            builder.setFeatureFlag("notifications.enabled", withValue: false)
        }
        
        JitsiMeet.sharedInstance().defaultConferenceOptions = defaultOptions
        
        let jitsiMeetView = JitsiMeetView()
        DispatchQueue.main.async {
            JMView = jitsiMeetView
        }
        JMDel.viewSelf = self
        
        let options = JitsiMeetConferenceOptions.fromBuilder { (builder) in
            builder.room = self.jitsi_room
            builder.setFeatureFlag("invite.enabled", withBoolean: false)
            builder.setFeatureFlag("pip.enabled", withValue: false)
            builder.setFeatureFlag("recording.enabled", withValue: false)
            builder.setFeatureFlag("close-captions.enabled", withValue: false)
            builder.setFeatureFlag("notifications.enabled", withValue: false)
            
        }
        
        jitsiMeetView.join(options)
        
        JMDel.JitsiView = jitsiMeetView
        
        jitsiMeetView.delegate = JMDel
        
        return jitsiMeetView
        
    }
    
    public func updateUIView(_ uiView: JitsiMeetView , context: Context) {

    }
    
}

class JMDelegate: NSObject, JitsiMeetViewDelegate {
    
    init(jitsiView: JitsiMeetView?, viewSelf: JitsiEmbed?) {
        
        self.JitsiView = jitsiView
        
        self.viewSelf = viewSelf
        
    }
    
    var JitsiView: JitsiMeetView?
    var viewSelf: JitsiEmbed?
    
    func conferenceTerminated(_ data: [AnyHashable : Any]!) {
        print("Leaving Conference")
        let data:[String: Int] = ["rc": 201]
        NotificationCenter.default.post(name: NSNotification.Name("JitsiEnd"), object: nil, userInfo: data)
        doLeave()
        
    }
    
    func participantLeft(_ data: [AnyHashable : Any]!) {
        print("Leave Participan")
        let data:[String: Int] = ["rc": 201]
        NotificationCenter.default.post(name: NSNotification.Name("ParticipanLeave"), object: nil, userInfo: data)
        doLeave()
    }
    
    func conferenceWillJoin(_ data: [AnyHashable : Any]!) {
        print("Will Joined")
    }
    
    func conferenceJoined(_ data: [AnyHashable : Any]!) {
        print("Joined")
        NotificationCenter.default.post(name: NSNotification.Name("ConferenceJoined"), object: nil)
    }
    
    func doLeave(){
        
        JitsiView?.leave()
        JitsiView?.hangUp()
        JitsiView?.removeFromSuperview()
        JitsiView = nil
    }
}
