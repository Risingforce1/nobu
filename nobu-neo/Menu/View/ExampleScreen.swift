//
//  ExampleScreen.swift
//  nobu-neo
//
//  Created by Prima Jatnika on 18/11/21.
//

import SwiftUI

struct ExampleScreen: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ExampleScreen_Previews: PreviewProvider {
    static var previews: some View {
        ExampleScreen()
    }
}
