//
//  JitsiScreen.swift
//  nobu-neo
//
//  Created by Prima Jatnika on 18/11/21.
//

import SwiftUI

struct JitsiScreen: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    @Binding var jitsiRoom: String
    @Binding var url: String
    @State var JitsiInstance: JitsiEmbed?
    
    @State var conferenceJoin: Bool = false
    
    var body: some View {
        ZStack {
            let JI_PIP = JitsiEmbed(
                jitsi_room: $jitsiRoom,
                status: .constant(""),
                url: $url)
            
            JI_PIP
                .onDisappear() {
                    JI_PIP.JMDel.doLeave()
                }
                .onReceive(NotificationCenter.default.publisher(for: NSNotification.Name("JitsiEnd"))) { obj in
                    presentationMode.wrappedValue.dismiss()
                }
                .onReceive(NotificationCenter.default.publisher(for: NSNotification.Name("ParticipanLeave"))) { obj in
                    presentationMode.wrappedValue.dismiss()
                }
            
//            if conferenceJoin {
//                EmptyView()
//            } else {
//                VStack {
//                    Image("WaitForOwnerDialog")
//                        .resizable()
//                    
//                    Button(action: { cancelJoin() }, label: {
//                        Text("Batalkan")
//                            .frame(width: 200, height: 30)
//                    })
//                }
//                .padding(.bottom, 20)
//                .frame(width: 300, height: 280, alignment: .center)
//                .background(Color.white)
//                .cornerRadius(10)
//            }
        }
        .navigationBarHidden(true)
        .edgesIgnoringSafeArea(.all)
        .onReceive(NotificationCenter.default.publisher(for: NSNotification.Name("ConferenceJoined"))) { obj in
            self.conferenceJoin = true
        }
    }
}

struct JitsiScreen_Previews: PreviewProvider {
    static var previews: some View {
        JitsiScreen(jitsiRoom: .constant(""), url: .constant(""))
    }
}

extension JitsiScreen {
    
    func cancelJoin() {
        let data:[String: Int] = ["rc": 0]
        NotificationCenter.default.post(name: NSNotification.Name("JitsiEnd"), object: nil, userInfo: data)
    }
    
}
