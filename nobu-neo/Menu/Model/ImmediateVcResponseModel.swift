//
//  ImmediateVcResponseModel.swift
//  nobu-neo
//
//  Created by Prima Jatnika on 18/11/21.
//

import Foundation

// MARK: - ImmediateVcResponseModel
struct ImmediateVcResponseModel: Codable {
    let code, message, roomName, status: String?
    let timestamp: String?
}
