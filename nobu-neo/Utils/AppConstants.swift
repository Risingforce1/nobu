//
//  AppConstants.swift
//  nobu-neo
//
//  Created by Prima Jatnika on 18/11/21.
//

import Foundation

class AppConstants {
    
    var BASE_URL_OCP = "https://portalekyc-dev.nobubank.com/eagle/api/v1.0"
    var BASE_URL_GCP = "https://ekyc.visiondg.xyz/eagle/api/v4.0"
    
    var VCS_URL_OCP = "https://vcs-dev.nobubank.com"
    var VCS_URL_GCP = "https://meet.visiondg.xyz"
}
