//
//  ErrorResult.swift
//  nobu-neo
//
//  Created by Prima Jatnika on 18/11/21.
//

import Foundation

enum ErrorResult: Error {
    case network(string: String)
    case parser(string: String)
    case code(code: Int)
    case customWithStatus(code: Int, codeStatus: String)
    case customWithMsg(code: Int, message: String)
    case customCodeWithMsg(code: String, message: String)
}

